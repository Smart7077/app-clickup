import SockJS from "sockjs-client";
import {Stomp} from "@stomp/stompjs";

var stompClient = null;

export function connect() {
    // setText('');
    var socket = new SockJS('/chat-messaging');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log("connected: " + frame);
        stompClient.subscribe('/chat/messages', function (response) {
            var data = JSON.parse(response.body);
            console.log(data)
            // setText(data.message);
            // draw("left", data.message);
        });
    });
}

export function disconnect() {
    stompClient.disconnect();
}

export function sendMessage(message) {
    stompClient.send("/app/message", {}
        , JSON.stringify({message}));

}
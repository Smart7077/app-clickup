import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SockJS from "sockjs-client";
import {Stomp} from "@stomp/stompjs";
import {Button} from "antd";


var stompClient = null;

function sendMessage() {
    stompClient.send("/app/message", {}
        , JSON.stringify({'name': 'saadasdksapodkasopd'}));

}

class Demo extends Component {
    componentDidMount() {
        function connect() {

            var socket = new SockJS('/chat-messaging');
            stompClient = Stomp.over(socket);
            stompClient.connect({}, function (frame) {
                console.log("connected: " + frame);
                stompClient.subscribe('/chat/messages', function (response) {
                    var data = JSON.parse(response.body);
                    console.log(data)
                });
            });
        }
    }


    render() {
        return (
            <div>
                <h1>Salom</h1>
                <Button onClick={sendMessage}>Push</Button>
            </div>
        );
    }
}

Demo.propTypes = {};

export default Demo;
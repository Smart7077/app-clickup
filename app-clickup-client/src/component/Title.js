import React, {useContext, useState} from "react";
import {Typography} from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";
import {makeStyles} from "@material-ui/core/styles";
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import {MoreHoriz} from "@material-ui/icons";
import AddIcon from '@material-ui/icons/Add';
import storeApi from "../utils/storeApi";

const useStyle = makeStyles((theme) => ({
    root: {
        width: '300px',
        backgroundColor: '#EBECF0',
        marginLeft: theme.spacing(1)
    },
    editableTitle: {
        flexGrow: 1,
        fontSize: '1.2rem',
        fontWeight: 'bold'
    },
    editableTitleContainer: {
        margin: theme.spacing(1),
        display: 'flex'
    },
    input: {
        '&:focus': {
            background: '#ddd'
        }
    }
}))

export default function Title({title, listId}) {
    const [open, setOpen] = useState(false);
    const [newTitle, setNewTitle] = useState(title);
    const {editListTitle} = useContext(storeApi);
    const classes = useStyle();
    const handleOnchange = (e) => {
        setNewTitle(e.target.value)
    };
    const handleOnBlur = () => {
        editListTitle(newTitle, listId)
        setOpen(false)
    };
    return (
        <div>
            {open ?
                <div>
                    <InputBase
                        value={newTitle}
                        inputProps={{
                            className: classes.input
                        }}
                        fullWidth
                        onChange={handleOnchange}
                        onBlur={handleOnBlur}
                    />
                </div>
                :
                <div className={classes.editableTitleContainer}>
                    <Typography
                        onClick={() => setOpen(true)}
                        className={classes.editableTitle}
                    >
                        {title}
                    </Typography>
                    <MoreHoriz/>
                    <AddIcon/>
                </div>

            }
        </div>
    )
}
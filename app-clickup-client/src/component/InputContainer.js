import React, {Component, useState} from 'react';
import Paper from "@material-ui/core/Paper";
import {Collapse, Typography} from "@material-ui/core";
import {makeStyles, fade} from "@material-ui/core/styles";
import InputCard from "./InputCard";


const useStyle = makeStyles((theme) => ({
    root: {
        marginTop: theme.spacing(1)
    },
    addCard: {
        padding: theme.spacing(1, 1, 1, 2),
        margin: theme.spacing(0, 1, 1, 1),
        background: '#EBECF0',
        '&:hover': {
            backgroundColor: fade('#000', 0.25)
        }
    }
}))


function InputContainer({listId, type}) {
    const classes = useStyle();
    const [open, setOpen] = useState(false);
    return (
        <div className={classes.root}>
            <Collapse in={open}>
                <InputCard type={type} listId={listId} setOpen={setOpen}/>
            </Collapse>
            <Collapse in={!open}>
                <Paper
                    onClick={() => setOpen(!open)}
                    className={classes.addCard}
                    elevation={0}>
                    <Typography>{type === 'card' ? "+ ADD CARD" : "+ ADD STATUS"}</Typography>
                </Paper>
            </Collapse>

        </div>
    );
}

export default InputContainer;
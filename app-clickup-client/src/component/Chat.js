import {Row, Form, Button, Input, Col} from "antd";
import {sendMessage} from "../utils/ws";
import {connect} from "../utils/ws";
import React, {useEffect} from "react";

function Chat() {
    // useEffect(() => {
    //     connect();
    // })
    const save = (e) => {
        console.log(e)
        sendMessage();
    }
    return (
        <div>
            <div>
                <Row>
                    <Form initialValues={{remember: true}}
                          onFinish={save}>
                        <Col span={15}>
                            <Form.Item label="Phone Number" name="text">
                                <Input name="text"/>
                            </Form.Item>
                        </Col>
                        <Col span={2}>
                            <Button type="primary" htmlType="submit">Save</Button>
                        </Col>
                    </Form>
                </Row>
            </div>
        </div>

    )
}

export default Chat;
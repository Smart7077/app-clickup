import React, {Component, useContext, useEffect, useState} from 'react';
import List from "./List";
import store from '../utils/store'
import StoreApi from '../utils/storeApi'
import {v4 as uuid} from 'uuid';
import {fade, makeStyles} from "@material-ui/core/styles";
import InputContainer from "./InputContainer";
import {DragDropContext, Droppable} from "react-beautiful-dnd";
import {connect, sendMessage} from "../utils/SocketConnect";
import SockJS from "sockjs-client";
import {Stomp} from "@stomp/stompjs";
import {Button} from "antd";
import axios from "axios";
import finalPropsSelectorFactory from "react-redux/lib/connect/selectorFactory";

// import openSocket from 'socket.io-client';

// const socket = openSocket('http://localhost:3000/component/dashboard');

var stompClient = null;

const useStyle = makeStyles((theme) => ({
    root: {
        display: 'flex',
    }
}))


function Dashboard(props) {
    const [data, setData] = useState([]);
    const classes = useStyle();
    const [statusList, setStatusList] = useState(props.location.state.statusList);

    const [task, setTask] = useState({});
    const [name, setName] = useState('');

    const [taskList, setTaskList] = useState([]);


    useEffect(() => {
        // axios.defaults.headers.common['Authorization'] = localStorage.getItem('app-clickUp');
        // axios.get('/api/status').then(res => {
        //     console.log(res);
        // })
        // axios({
        //     headers: {
        //         Authorization: localStorage.getItem('app-clickUp')
        //     },
        //     url: '/api/status',
        //     method: 'GET'
        // }).then(res => {
        //     if (res.success) {
        //         setData(res.data);
        //     }
        // })

        // axios({
        //     headers: {
        //         Authorization: localStorage.getItem('app-clickUp')
        //     },
        //     url: '/api/task',
        //     method: 'GET'
        // }).then(res => {
        //     if (res.success) {
        //         setTask(res.data);
        //     }
        // })

        // var socket = new SockJS('/chat-messaging');
        // stompClient = Stomp.over(socket);
        // stompClient.connect({}, function (frame) {
        //     console.log("connected: " + frame);
        //     stompClient.subscribe('/tasks/get', function (response) {
        //         var data = JSON.parse(response.body);
        //         console.log(data)
        //     });
        // });
        // stompClient.debug();

    })

    function connect() {
        var socket = new SockJS('/chat-messaging');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log("connected: " + frame);
            stompClient.subscribe('/chat/messages', function (response) {
                var data = JSON.parse(response.body);
                console.log(data)
                setTask(data.body);
            });
        });
    }

    function sendMessage() {
        stompClient.send("/app/message", {}
            , JSON.stringify({'name': name}));

    }


    const addMoreCard = (title, listId) => {
        console.log(title)
        setName(title)
        sendMessage();

        // const newCardId = uuid();
        // const newCard = {
        //     id: newCardId,
        //     title: title
        // }
        // const list = data.lists[listId];
        // list.cards = [...list.cards, newCard]
        // const newState = {
        //     ...data,
        //     lists: {
        //         ...data.lists,
        //         [listId]: list
        //     }
        // }
        //
        // setData(newState)
        if (task.id) {
            console.log('ishladi')
        }
    }

    const addList = (title) => {
        const newListid = uuid();
        const newList = {
            id: newListid,
            title,
            cards: []
        };
        const newState = {
            listIds: [...data.listIds, newListid],
            lists: {
                ...data.lists,
                [newListid]: newList
            }
        }
        setData(newState);
    }

    const editListTitle = (title, listId) => {
        const list = data.lists[listId];
        list.title = title;
        const newState = {
            ...data,
            lists: {
                ...data.lists,
                [listId]: list
            }
        };
        setData(newState)
    };


    const onDragEnd = (result) => {
        console.log(result)
        const {destination, source, draggableId, type} = result;

        console.log("Destination ", destination, " Source ", source, draggableId)
        if (!destination) {
            return;
        }
        // if (type === "list") {
        //     const newListId = data.listIds;
        //     newListId.splice(source.index, 1);
        //     newListId.splice(destination.index, 0, draggableId)
        //     return;
        // }
        const sourceList = statusList[source.droppableId - 1];
        const destinationList = statusList[destination.droppableId - 1];
        const draggingCard = sourceList.tasks.filter(
            (task) => task.id == draggableId
        )[0];


        if (source.droppableId == destination.droppableId) {
            sourceList.tasks.splice(source.index, 1);
            destinationList.tasks.splice(destination.index, 0, draggingCard);
            // const newState = {
            //     ...statusList,
            //     lists: {
            //         ...statusList.tasks,
            //         [sourceList.id]: destinationList,
            //     },
            // };
            // setStatusList(newState)
        } else {
            sourceList.tasks.splice(source.index, 1);
            destinationList.tasks.splice(destination.index, 0, draggingCard);
            // const newState = {
            //     ...statusList,
            //     lists: {
            //         ...statusList.tasks,
            //         [sourceList.id]: sourceList,
            //         [destinationList.id]: destinationList
            //     }
            // };
            // setData(newState)
        }

        //
        // if (source.droppableId == destination.droppableId) {
        //     sourceList.cards.splice(source.index, 1);
        //     destinationList.cards.splice(destination.index, 0, draggingCard);
        //     const newState = {
        //         ...data,
        //         lists: {
        //             ...data.lists,
        //             [sourceList.id]: destinationList,
        //         },
        //     };
        //     setData(newState)
        // } else {
        //     sourceList.cards.splice(source.index, 1);
        //     destinationList.cards.splice(destination.index, 0, draggingCard);
        //     const newState = {
        //         ...data,
        //         lists: {
        //             ...data.lists,
        //             [sourceList.id]: sourceList,
        //             [destinationList.id]: destinationList
        //         }
        //     };
        //     setData(newState)
        // }
    }

    return (
        <StoreApi.Provider value={{addMoreCard, addList, editListTitle}}>
            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="app" type="list" direction="horizontal">
                    {(provided) => (
                        <div
                            className={classes.root}
                            ref={provided.innerRef}
                            {...provided.droppableProps}

                        >
                            {statusList.length > 0 ?
                                statusList.map((status, index) => {
                                    // const list = statusList[listId];
                                    return <List list={status} key={status.id} index={index}/>
                                })
                                : ''}
                            {/*{data.listIds.map((listId, index) => {*/}
                            {/*    const list = data.lists[listId];*/}
                            {/*    return <List list={list} key={listId} index={index}/>*/}
                            {/*})}*/}
                            <InputContainer type="list"/>
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>

            </DragDropContext>

        </StoreApi.Provider>

    );
}

export default Dashboard;
import React from "react";
import {Paper, Typography, CssBaseline} from "@material-ui/core";
import {Pages} from "@material-ui/icons";
import {makeStyles} from "@material-ui/core/styles";
import Board from 'react-trello'
import Title from "./Title";
import Card from "./Card";
import InputContainer from "./InputContainer";
import {Draggable, Droppable} from "react-beautiful-dnd";

const useStyle = makeStyles((theme) => ({
    root: {
        width: '300px',
        backgroundColor: '#EBECF0',
        marginLeft: theme.spacing(1)
    },
    cardContainer: {
        marginTop: theme.spacing(4)
    }
}))


function List({list, index}) {
    const classes = useStyle();
    console.log(list)
    return (
        <Draggable draggableId={list.id} index={index}>
            {(provided) => (
                <div {...provided.draggableProps} ref={provided.innerRef}>
                    <Paper className={classes.root} {...provided.dragHandleProps}>
                        <CssBaseline/>
                        <Title title={list.nameUz} listId={list.id}/>
                        <Droppable droppableId={list.id}>
                            {(provided) => (
                                <div
                                    ref={provided.innerRef}
                                    {...provided.droppableProps}
                                    className={classes.cardContainer}>
                                    {list.tasks.map((card, index) =>
                                        <Card key={index + 1} index={index} card={card} listId={list.id}/>
                                    )}
                                    {provided.placeholder}
                                </div>
                            )}

                        </Droppable>

                        <InputContainer type="card" listId={list.id}/>
                    </Paper>
                </div>
            )}
        </Draggable>
    )
}

export default List;
import React, {Component, useContext, useState} from 'react';
import InputBase from "@material-ui/core/InputBase";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import {IconButton} from "@material-ui/core";
import ClearIcon from '@material-ui/icons/Clear';
import {fade, makeStyles} from "@material-ui/core/styles";
import storeApi from "../utils/storeApi";


const useStyle = makeStyles((theme) => ({
    card: {
        margin: theme.spacing(0, 1, 1, 1),
        paddingBottom: theme.spacing(4)
    },
    input: {
        margin: theme.spacing(1)
    },
    btnConfirm: {
        background: '#5AAC44',
        color: '#fff',
        "&:hover": {
            background: fade('#5AAC44', 0.75)
        }
    },
    confirm: {
        margin: theme.spacing(0, 1, 1, 1),
    }
}))

function InputCard({setOpen, listId, type}) {
    const classes = useStyle();
    const [title, setTitle] = useState('');
    const {addMoreCard, addList} = useContext(storeApi);

    const handleOnchange = (e) => {
        setTitle(e.target.value);
    }
    const handleBtnConfirm = () => {
        if (type === 'card') {
            addMoreCard(title, listId);
            setTitle('')
            setOpen(false)
        } else {
            addList(title)
            setTitle('')
        }

    }
    const handleOnBlur = () => {
        setOpen(false);
        // setTitle('')
    }
    return (
        <div>
            <div>
                <Paper className={classes.card}>
                    <InputBase
                        onChange={handleOnchange}
                        multiline
                        fullWidth
                        inputProps={{
                            className: classes.input
                        }}
                        value={title}
                        onBlur={handleOnBlur}
                        placeholder={type === 'card' ? "Enter Task title" : "Enter Status Name"}
                    />

                </Paper>
            </div>
            <div className={classes.confirm}>
                <Button className={classes.btnConfirm}
                        onClick={handleBtnConfirm}>{type === 'card' ? "Add Card" : "ADD LIST"}</Button>
                <IconButton onClick={() => setOpen(false)}>
                    <ClearIcon/>
                </IconButton>
            </div>

        </div>
    );
}

export default InputCard;
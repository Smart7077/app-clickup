import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Card, Col, Form, Input, Row} from "antd";
import {connect} from "react-redux";
import axios from "axios";
import {withRouter} from 'react-router-dom';

class Login extends Component {
    render() {
        const {history} = this.props


        const login = (v) => {
            axios({
                url: '/api/auth/login',
                method: 'POST',
                data: v
            }).then(res => {
                if (res) {
                    console.log(res)

                    localStorage.setItem("app-clickUp", res.data.tokenType + " " + res.data.tokenBody)
                    history.push('/component/dashboard')
                }
            })
        }
        return (
            <Row>
                <Col span={12} offset={4}>
                    <Card className="text-center">
                        <h2 className="t">Cabinet</h2><br/>
                        <Form labelCol={{span: 8}} initialValues={{remember: true}} wrapperCol={{span: 14}}
                              onFinish={login}>
                            <Form.Item label="Email" name="email">
                                <Input type="email" name="email"/>
                            </Form.Item>
                            <Form.Item label="Password" name="password">
                                <Input.Password name="password"/>
                            </Form.Item>
                            <Button type="primary" htmlType="submit">Kirish</Button>
                        </Form>
                    </Card>
                </Col>
            </Row>
        );
    }
}

Login.propTypes = {};

export default withRouter(Login);

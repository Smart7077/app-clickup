import React, {Component, useState} from 'react';
import './style.css';
import SockJS from 'sockjs-client'
import {Stomp} from '@stomp/stompjs'
import {Form, Input} from "antd";


var stompClient = null;


function ChatDemo() {
    const [text, setText] = useState('');
    const [message, setMessage] = useState('');

    function connect() {
        setText('');
        var socket = new SockJS('/chat-messaging');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log("connected: " + frame);
            stompClient.subscribe('/chat/messages', function (response) {
                var data = JSON.parse(response.body);
                console.log(data)
                setText(data.message);
                // draw("left", data.message);
            });
        });
    }

    function disconnect() {
        stompClient.disconnect();
    }

    function sendMessage() {
        console.log(message)
        stompClient.send("/app/message", {}
            , JSON.stringify({'message': message}));

    }

    const write = (v) => {
        setMessage(v.target.value)
    }

    return (
        <div>
            <h1>Simple chat</h1>
            <Form>

                <div className="chat_window">
                    <div className="top_menu">
                        <div className="buttons">
                            <div className="button close"></div>
                            <div className="button minimize"></div>
                            <div className="button maximize"></div>
                        </div>
                        <div className="title">Chat</div>
                    </div>
                    <ul className="messages">{text}</ul>
                    <div className="bottom_wrapper clearfix">
                        <div className="message_input_wrapper">
                            <Form.Item name="message">
                                <Input onChange={write} className="message_input" name="message"/>
                            </Form.Item>
                            {/*<input type="text" id="from" className="message_input"*/}
                            {/*       placeholder="Type your message here..."/>*/}
                        </div>
                        <div className="send_message">
                            <div className="icon"></div>
                        </div>

                        <button onClick={connect}>Connect to chat</button>
                        <button onClick={sendMessage} className="text">Send</button>
                        <button onClick={disconnect}>Disconnect from chat</button>
                    </div>
                </div>
                <div id="message_template" className="message_template">
                    <li className="message">
                        <div className="avatar"></div>
                        <div className="text_wrapper">
                            <div className="text"></div>
                        </div>
                    </li>
                </div>
            </Form>

        </div>
    )
}

export default ChatDemo;
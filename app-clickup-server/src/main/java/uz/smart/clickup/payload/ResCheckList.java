package uz.smart.clickup.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResCheckList {
    private Integer id;
    private String nameUz;
    private String nameRu;
    private String nameEn;


}

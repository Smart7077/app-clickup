package uz.smart.clickup.payload;

import lombok.Data;
import uz.smart.clickup.entity.TaskList;

@Data
public class ReqTask {
    private String name;
    private String description;
    private Integer taskListId;
    private String subTask;
}

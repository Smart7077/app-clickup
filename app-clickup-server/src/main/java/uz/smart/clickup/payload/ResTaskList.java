package uz.smart.clickup.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import uz.smart.clickup.entity.Status;

import java.util.List;

@Data
@AllArgsConstructor
public class ResTaskList {
    private Integer id;
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private List<ResStatus> statusList;
}

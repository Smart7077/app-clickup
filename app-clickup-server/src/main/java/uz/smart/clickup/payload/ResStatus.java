package uz.smart.clickup.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.smart.clickup.entity.Task;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResStatus {
    private Integer Id;
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private List<ResTask> tasks;
}

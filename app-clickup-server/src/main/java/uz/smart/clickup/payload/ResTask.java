package uz.smart.clickup.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.smart.clickup.entity.Attachment;
import uz.smart.clickup.entity.CheckList;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResTask {
    private UUID id;
    private String name;
    private String description;
    private Integer statusId;
    private String subTask;
    private List<UUID> attachmentsId;
    private List<ResCheckList> checkLists;
    private Timestamp startDate;
    private Timestamp dueDate;
}

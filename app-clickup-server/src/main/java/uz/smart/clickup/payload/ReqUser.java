package uz.smart.clickup.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqUser {
    private UUID id;
    private String fullName;
    private String email;
    private String password;
}

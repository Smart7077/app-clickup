package uz.smart.clickup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.smart.clickup.entity.Permission;

public interface PermissionRepository extends JpaRepository<Permission, Integer> {
}

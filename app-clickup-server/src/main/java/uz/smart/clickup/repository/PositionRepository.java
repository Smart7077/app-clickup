package uz.smart.clickup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.smart.clickup.entity.Position;

public interface PositionRepository extends JpaRepository<Position, Integer> {
}

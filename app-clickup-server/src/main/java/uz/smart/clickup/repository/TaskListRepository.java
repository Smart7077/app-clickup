package uz.smart.clickup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.smart.clickup.entity.TaskList;

import java.util.List;

public interface TaskListRepository extends JpaRepository<TaskList, Integer> {


}

package uz.smart.clickup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.smart.clickup.entity.Attachment;


import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
}

package uz.smart.clickup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.smart.clickup.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}

package uz.smart.clickup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.smart.clickup.entity.CheckList;

public interface CheckListRepository extends JpaRepository<CheckList, Integer> {
}

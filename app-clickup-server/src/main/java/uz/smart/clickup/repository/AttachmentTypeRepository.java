package uz.smart.clickup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.smart.clickup.entity.AttachmentType;

import java.util.UUID;

public interface AttachmentTypeRepository extends JpaRepository<AttachmentType, UUID> {
}

package uz.smart.clickup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.smart.clickup.entity.Status;

public interface StatusRepository extends JpaRepository<Status, Integer> {
}

package uz.smart.clickup.security;

//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseAuthException;
//import com.google.firebase.auth.UserRecord;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.smart.clickup.repository.AttachmentRepository;
import uz.smart.clickup.repository.PositionRepository;
import uz.smart.clickup.repository.RoleRepository;
import uz.smart.clickup.repository.UserRepository;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    final
    UserRepository userRepository;
    final
    RoleRepository roleRepository;
    final
    PasswordEncoder passwordEncoder;
    final
    PositionRepository positionRepository;
    final
    AttachmentRepository attachmentRepository;

    public AuthService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder, PositionRepository positionRepository, AttachmentRepository attachmentRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.positionRepository = positionRepository;
        this.attachmentRepository = attachmentRepository;
    }

//    public ApiResponse createUser(ReqUser reqUser) {
//        if (!userRepository.existsByPhoneNumber(reqUser.getPhoneNumber())) {
//            if (validBirthDate(reqUser.getBirthDate())) {
//                User user = new User();
//                user.setPosition(positionRepository.getOne(reqUser.getPositionId()));
//                user.setFirstName(reqUser.getFirstName());
//                user.setLastName(reqUser.getLastName());
//                user.setMiddleName(reqUser.getMiddleName());
//                user.setUserName(reqUser.getUserName());
//                user.setBirthDate(reqUser.getBirthDate());
//                user.setPhoneNumber(reqUser.getPhoneNumber());
//                user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
//                user.setPassport(reqUser.getPassport());
//                user.setRoles(Collections.singletonList(roleRepository.findByName(RoleName.ROLE_USER)));
//                user.setContact(contactService.addContact(reqUser.getReqContact(), null));
//                user.setUniversity(reqUser.getUniversityId() == null ? null : universityRepository.getOne(reqUser.getUniversityId()));
//                userRepository.save(user);
//                return new ApiResponse("User yaratildi", true);
//            }
//            return new ApiResponse("Bu user ro'yxatdan o'tgan", false);
//
//        }
//        return new ApiResponse("Yoshingiz to'g'ri kelmadi !", false);
//    }
//
//    public ApiResponse editUser(UUID id, ReqUser reqUser) {
//        try {
//            Optional<User> optionalUser = userRepository.findById(id);
//            if (optionalUser.isPresent()) {
//                User user = new User();
//                user.setPosition(positionRepository.getOne(reqUser.getPositionId()));
//                user.setFirstName(reqUser.getFirstName());
//                user.setLastName(reqUser.getLastName());
//                user.setMiddleName(reqUser.getMiddleName());
//                user.setUserName(reqUser.getUserName());
//                user.setPhoneNumber(reqUser.getPhoneNumber());
//                user.setPassport(reqUser.getPassport());
//                user.setBirthDate(reqUser.getBirthDate());
//                user.setPassword(reqUser.getPassword());
//                user.setUniversity(universityRepository.getOne(reqUser.getUniversityId()));
//                user.setContact(contactService.addContact(reqUser.getReqContact(), user.getContact()));
//                userRepository.save(user);
//                return new ApiResponse("User o'zgartirildi !!", true);
//            }
//
//        } catch (Exception e) {
//            return new ApiResponse("Bunday User topilmadi", false);
//
//        }
//        return null;
//    }
//
//    ;

//    public ApiResponse addUser(ReqUser reqUser) {
//        if (getCheckUserSignUp(reqUser.getPhoneNumber()))
//            if (!userRepository.existsByPhoneNumber(reqUser.getPhoneNumber())) {
//                if (validBirthDate(reqUser.getBirthDate())) {
//                    User user = new User();
//                    user.setFirstName(reqUser.getFirstName());
//                    user.setLastName(reqUser.getLastName());
//                    user.setPhoneNumber(reqUser.getPhoneNumber());
//                    user.setBirthDate(reqUser.getBirthDate());
//                    user.setEmail(reqUser.getEmail());
//                    user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
//                    user.setRoles(Collections.singletonList(roleRepository.findByName(RoleName.ROLE_USER)));
//                    userRepository.save(user);
//                    return new ApiResponse("User ro'yxatdan o'tkazildi", true);
//                }
//                return new ApiResponse("Yoshingiz mos emas", false);
//            }
//        return new ApiResponse("Bunday telefon raqamli user sistemada mavjud", false);
//    }

//    public ApiResponse editUser(UUID id, ReqUser reqUser) {
//        try {
//            Optional<User> optionalUser = userRepository.findById(id);
//            if (optionalUser.isPresent()) {
//                User user = new User();
//                user.setFirstName(reqUser.getFirstName());
//                user.setLastName(reqUser.getLastName());
//                user.setPhoneNumber(reqUser.getPhoneNumber());
//                user.setBirthDate(reqUser.getBirthDate());
//                user.setEmail(reqUser.getEmail());
//                user.setPassword(reqUser.getPassword());
//                userRepository.save(user);
//                return new ApiResponse("User o'zgartirildi!!", true);
//            }
//            return new ApiResponse("User o'zgartirildi!!", true);
//        } catch (Exception e) {
//            return new ApiResponse("Bunday User topilmadi", false);
//        }
//    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByEmail(s).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }

    public UserDetails loadUserByUserId(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }

//    public Boolean getCheckUserSignUp(String phoneNumber) {
//        try {
//            UserRecord userRecord = FirebaseAuth.getInstance().getUserByPhoneNumber(phoneNumber);
//            if (userRecord != null)
//                return !userRepository.existsByPhoneNumber(phoneNumber);
//            return false;
//        } catch (FirebaseAuthException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }


    public boolean validBirthDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        Integer birthDate = Integer.valueOf(simpleDateFormat.format(date));
        Integer now = Integer.valueOf(simpleDateFormat.format(new Date()));
        return now - birthDate >= 16;
    }


}

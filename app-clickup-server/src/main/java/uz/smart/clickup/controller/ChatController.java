package uz.smart.clickup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.smart.clickup.entity.Task;
import uz.smart.clickup.payload.ReqTask;
import uz.smart.clickup.repository.StatusRepository;
import uz.smart.clickup.repository.TaskListRepository;
import uz.smart.clickup.repository.TaskRepository;

@RestController
@RequestMapping("/api/chat")
public class ChatController {

    @Autowired
    TaskListRepository taskListRepository;
    @Autowired
    TaskRepository taskRepository;
    @Autowired
    StatusRepository statusRepository;

    @MessageMapping("/message")
    @SendTo("/chat/messages")
    public HttpEntity<?> addTask(ReqTask reqTask) {
        Task saveTask = new Task();
        saveTask.setName(reqTask.getName());
        saveTask.setStatus(statusRepository.getOne(2));
        Task save = taskRepository.save(saveTask);

        return ResponseEntity.ok(save);

    }

    @SubscribeMapping("/tasks/get")
    public HttpEntity<?> getTasks() {
        return ResponseEntity.ok(taskRepository.findAll());
    }

}

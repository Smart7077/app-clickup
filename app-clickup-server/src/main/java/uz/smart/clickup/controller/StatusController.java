package uz.smart.clickup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.smart.clickup.repository.StatusRepository;

@RestController
@RequestMapping("/api/status")
public class StatusController {

    @Autowired
    StatusRepository statusRepository;

    @GetMapping
    public HttpEntity<?> getStatus() {
        return ResponseEntity.ok(statusRepository.findAll());
    }
}

package uz.smart.clickup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.smart.clickup.entity.Task;
import uz.smart.clickup.repository.TaskRepository;

@RestController
@RequestMapping("/api/task")
public class TaskController {
    @Autowired
    TaskRepository taskRepository;


    @GetMapping
    public HttpEntity<?> getTasks() {
        return ResponseEntity.ok(taskRepository.findAll());
    }
}

package uz.smart.clickup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.smart.clickup.entity.TaskList;
import uz.smart.clickup.repository.TaskListRepository;
import uz.smart.clickup.service.TaskListService;

import java.nio.file.ReadOnlyFileSystemException;

@RestController
@RequestMapping("/api/list")
public class ListController {

    @Autowired
    TaskListRepository taskListRepository;
    @Autowired
    TaskListService taskListService;

    @GetMapping
    public HttpEntity<?> getLists() {
        return ResponseEntity.ok(taskListService.getLists());
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getListById(@PathVariable Integer id) {
        TaskList taskList = taskListRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("get taskList"));

        return ResponseEntity.ok(taskList);
    }

}

package uz.smart.clickup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.smart.clickup.entity.CheckList;
import uz.smart.clickup.entity.Status;
import uz.smart.clickup.entity.Task;
import uz.smart.clickup.entity.TaskList;
import uz.smart.clickup.entity.template.AbsEntity;
import uz.smart.clickup.entity.template.AbsNameEntity;
import uz.smart.clickup.payload.*;
import uz.smart.clickup.repository.TaskListRepository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class TaskListService {
    private ConcurrentHashMap<String, List<String>> map = new ConcurrentHashMap<>();

    @Autowired
    TaskListRepository taskListRepository;

    public ApiResponse getLists() {
        List<ResTaskList> resTaskList = taskListRepository.findAll().stream().map(this::getTaskList).collect(Collectors.toList());
        return new ApiResponse("Select All", true, resTaskList);
    }

    public ResTaskList getTaskList(TaskList taskList) {
        return new ResTaskList(
                taskList.getId(),
                taskList.getNameUz(),
                taskList.getNameRu(),
                taskList.getNameEn(),
                taskList.getStatusList().stream().map(this::getStatus).collect(Collectors.toList())
        );
    }

    ;

    public ResStatus getStatus(Status status) {
        return new ResStatus(
                status.getId(),
                status.getNameUz(),
                status.getNameRu(),
                status.getNameEn(),
                status.getTasks().stream().map(this::getTask).collect(Collectors.toList())
        );
    }

    public ResTask getTask(Task task) {
        return new ResTask(
                task.getId(),
                task.getName(),
                task.getDescription(),
                task.getStatus().getId(),
                task.getSubTask(),
                task.getAttachments().stream().map(AbsEntity::getId).collect(Collectors.toList()),
                task.getCheckLists().stream().map(this::getCheckList).collect(Collectors.toList()),
                task.getStartDate(),
                task.getDueDate()
        );
    }

    public ResCheckList getCheckList(CheckList checkList) {
        return new ResCheckList(
                checkList.getId(),
                checkList.getNameUz(),
                checkList.getNameRu(),
                checkList.getNameEn()
        );
    }

    public ConcurrentHashMap<String, List<String>> getList(Integer id) {
//        taskListRepository.findById(id).forEach(taskList -> {
//            if (taskList.getStatusList() != null) {
//                map.put(taskList.getNameUz(),
//                        taskList
//                                .getStatusList()
//                                .stream()
//                                .map(Status::getNameUz)
//                                .collect(Collectors.toList())
//
//                );
//            }
//        });

        return map;
    }
}

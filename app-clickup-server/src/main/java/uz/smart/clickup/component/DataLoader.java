package uz.smart.clickup.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.smart.clickup.entity.User;
import uz.smart.clickup.repository.AttachmentTypeRepository;
import uz.smart.clickup.repository.PermissionRepository;
import uz.smart.clickup.repository.RoleRepository;
import uz.smart.clickup.repository.UserRepository;

@Component
public class DataLoader implements CommandLineRunner {
    final
    UserRepository userRepository;
    final
    PasswordEncoder passwordEncoder;
    final
    RoleRepository roleRepository;
    final
    AttachmentTypeRepository attachmentTypeRepository;

    final
    PermissionRepository permissionRepository;

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    public DataLoader(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, AttachmentTypeRepository attachmentTypeRepository, PermissionRepository permissionRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.attachmentTypeRepository = attachmentTypeRepository;
        this.permissionRepository = permissionRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(
                    new User(
                            "Islom Xujanazarov",
                            "islomxujanazarov0501@gmail.com",
                            passwordEncoder.encode("1"),
                            roleRepository.findAll()
                    ));
//            attachmentTypeRepository.save(new AttachmentType("image/jpeg"))

        }
    }
}

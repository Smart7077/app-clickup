package uz.smart.clickup.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.smart.clickup.entity.template.AbsEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Attachment extends AbsEntity {
    private String name;
    private String contentType;
    private long sized;
}

package uz.smart.clickup.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.smart.clickup.entity.template.AbsNameEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Status extends AbsNameEntity {
    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "status")
    private List<Task> tasks;

}

package uz.smart.clickup.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.smart.clickup.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class Task extends AbsEntity {

    private String name;
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private Status status;

    private String subTask;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Attachment> attachments;

    @OneToMany(fetch = FetchType.LAZY)
    private List<CheckList> checkLists;

    private Timestamp startDate;
    private Timestamp dueDate;

}

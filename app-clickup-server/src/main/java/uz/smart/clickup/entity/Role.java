package uz.smart.clickup.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import uz.smart.clickup.entity.enums.RoleName;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(value = EnumType.STRING)
    private RoleName name;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "permissions",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id")
    )
    private List<Permission> permissions;

    @Override
    public String getAuthority() {
        return name.name();
    }
}

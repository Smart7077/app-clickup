package uz.smart.clickup.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.smart.clickup.entity.template.AbsNameEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Permission extends AbsNameEntity {

}

package uz.smart.clickup.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.smart.clickup.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class AttachmentContent extends AbsEntity {
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    private Attachment attachment;
    private byte[] bytes;
}

package uz.smart.clickup.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.smart.clickup.entity.template.AbsNameEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class TaskList extends AbsNameEntity {
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "List_Status",
            joinColumns = @JoinColumn(name = "taskList_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "status_id", referencedColumnName = "id")
    )
    private List<Status> statusList;
}
